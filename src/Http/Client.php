<?php declare(strict_types=1);

namespace JohnSear\HttpClient\Http;

use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use InvalidArgumentException;
use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;

abstract class Client
{
    /** @var ClientConfigurationInterface $configuration */
    private $configuration;
    /** @var ClientInterface */
    private $client;

    protected function getConfiguration(): ClientConfigurationInterface
    {
        return $this->configuration;
    }

    protected function setConfiguration(ClientConfigurationInterface $configuration): void
    {
        $this->configuration = $configuration;
    }

    /**
     * @throws InvalidArgumentException
     */
    protected function getClient(): ClientInterface
    {
        if(! $this->client instanceof ClientInterface) {
            $this->client = new GuzzleHttpClient();
        }

        return $this->client;
    }

    /**
     * @throws RuntimeException
     * @throws GuzzleException
     * @throws InvalidArgumentException
     */
    protected function request(string $request, string $type = 'GET', array $additionalOptions = []): array
    {
        $response = $this->requestRaw($request, $type, $additionalOptions);

        return $this->decodeJsonFromResponse($response);
    }

    /**
     * @throws GuzzleException
     * @throws InvalidArgumentException
     */
    protected function requestRaw(string $request, string $type = 'GET', array $additionalOptions = []): ResponseInterface
    {
        $client = $this->getClient();

        $options = array_merge($this->getPreparedRequestOptions(), $additionalOptions);

        return $client->request($type, $this->getPreparedUri($request), $options);
    }

    private function getPreparedUri(string $request): string
    {
        return  $this->getConfiguration()->getBaseUri() . '/' . ltrim($request, '/');
    }

    private function getPreparedRequestOptions(): array
    {
        $options = [];

        $options['headers']['Host'] = $this->getConfiguration()->getBaseHost();

        if($this->getConfiguration()->useAuthentication()) {
            $this->setAuthenticationToOptions($options);
        }

        return $options;
    }

    private function setAuthenticationToOptions(array &$options): void
    {
        if($this->getConfiguration()->getUseAuthToken()) {
            $authTokenKey = $this->getConfiguration()->getAuthTokenKey();
            $authToken    = $this->getConfiguration()->getAuthToken();

            $options['headers'][$authTokenKey] = $authToken;
        } else {
            $options['auth'] = [
                $this->getConfiguration()->getUsername(),
                $this->getConfiguration()->getPassword()
            ];
        }
    }

    /**
     * @throws RuntimeException
     */
    private function decodeJsonFromResponse(MessageInterface $response): array
    {
        $rawData = $response->getBody()->getContents();

        if($this->validateResponseAsJson($rawData)) {

            return json_decode($rawData, true);
        }

        return [];
    }

    private function validateResponseAsJson(string $response): bool
    {
        if (!empty($response)) {
            @json_decode($response, true);

            return (json_last_error() === JSON_ERROR_NONE);
        }

        return false;
    }
}
