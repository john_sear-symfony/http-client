<?php declare(strict_types=1);

namespace JohnSear\HttpClient\Http;

interface ClientConfigurationInterface
{
    public function getBaseHost(): string;

    public function setBaseApiHost(string $baseHost): ClientConfigurationInterface;

    public function getBaseApiPath(): string;

    public function setBaseApiPath(string $baseApiPath): ClientConfigurationInterface;

    public function getBaseUri(): string;

    public function getBaseUriSecure(): bool;

    public function setBaseUriSecure(bool $baseUriSecure): ClientConfigurationInterface;

    public function useAuthentication(): bool;

    public function setUseAuthentication(bool $useAuthentication): ClientConfigurationInterface;

    public function getAuthTokenKey(): string;

    public function setAuthTokenKey(string $authTokenKey): ClientConfigurationInterface;

    public function getAuthToken(): string;

    public function setAuthToken(string $authToken): ClientConfigurationInterface;

    public function getUseAuthToken(): bool;

    public function setUseAuthToken(bool $useAuthToken): ClientConfigurationInterface;

    public function getUsername(): string;

    public function setUsername(string $username): ClientConfigurationInterface;

    public function getPassword(): string;

    public function setPassword(string $password): ClientConfigurationInterface;
}
