<?php declare(strict_types=1);

namespace JohnSear\HttpClient\Http;

class ClientConfiguration implements ClientConfigurationInterface
{
    private $baseHost;
    private $baseApiPath;
    private $baseUri;
    private $baseUriSecure = false;
    private $useAuthentication;
    private $authTokenKey;
    private $authToken;
    private $useAuthToken = false;
    private $username;
    private $password;

    public function getBaseHost(): string
    {
        return $this->baseHost;
    }

    public function setBaseApiHost(string $baseHost): ClientConfigurationInterface
    {
        $this->baseHost = $baseHost;

        return $this;
    }

    public function getBaseApiPath(): string
    {
        return $this->baseApiPath;
    }

    public function setBaseApiPath(string $baseApiPath): ClientConfigurationInterface
    {
        $this->baseApiPath = $baseApiPath;

        return $this;
    }

    public function getBaseUri(): string
    {
        if($this->baseUri === null) {
            $protocol = ($this->getBaseUriSecure()) ? 'https' : 'http';
            $this->baseUri = $protocol . '://' . $this->getBaseHost() . $this->getBaseApiPath();
        }

        return $this->baseUri;
    }

    public function getBaseUriSecure(): bool
    {
        return $this->baseUriSecure;
    }

    public function setBaseUriSecure(bool $baseUriSecure): ClientConfigurationInterface
    {
        $this->baseUriSecure = $baseUriSecure;

        return $this;
    }

    public function useAuthentication(): bool
    {
        if($this->useAuthentication === null) {
            if($this->getUseAuthToken() || ($this->getUsername() != '' && $this->getPassword() != '')) {
                $this->useAuthentication = true;
            }
        }
        return (bool) $this->useAuthentication;
    }

    public function setUseAuthentication(bool $useAuthentication): ClientConfigurationInterface
    {
        $this->useAuthentication = $useAuthentication;

        return $this;
    }

    public function getAuthTokenKey(): string
    {
        return (string) $this->authTokenKey;
    }

    public function setAuthTokenKey(string $authTokenKey): ClientConfigurationInterface
    {
        $this->authTokenKey = $authTokenKey;

        return $this;
    }

    public function getAuthToken(): string
    {
        return (string) $this->authToken;
    }

    public function setAuthToken(string $authToken): ClientConfigurationInterface
    {
        $this->authToken = $authToken;

        return $this;
    }

    public function getUseAuthToken(): bool
    {
        return $this->useAuthToken;
    }

    public function setUseAuthToken(bool $useAuthToken): ClientConfigurationInterface
    {
        $this->useAuthToken = $useAuthToken;

        return $this;
    }

    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): ClientConfigurationInterface
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): ClientConfigurationInterface
    {
        $this->password = $password;

        return $this;
    }

}
